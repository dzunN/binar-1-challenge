function checkEmail(email) {
  // inisialisasi format email
  const emailFormat =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  //Cek apakah email sudah diinputkan

  if (!email) {
    //jika email tidak diisi
    return "Email harus diisi";
  }

  //jika email diisi
  if (emailFormat.test(email) && typeof email === "string") {
    return email + "Valid";
  } else {
    const regex = /=?@/;
    if (!regex.test(email)) {
      return "ERROR: Please include an '@' in the email";
    } else {
      return "INVALID";
    }
  }
}

// console.log(checkEmail("saktibima@binar.co.id"));
// console.log(checkEmail("saktibima@binar.com"));

// console.log(checkEmail("saktibima@binar"));
console.log(checkEmail("sakti@bima@binar.co.id"));

// console.log(checkTypeNumber(checkEmail(3322))); //Ini akan terkena Error, karena fungsi "checkTypeNumber()" tidak ada atau tidak terdefinisikan.

// console.log(checkEmail()); // <= Akan return undefined, karena fungsi "checkEmail()" tidak menerima nilai atau email tidak diinputkan.