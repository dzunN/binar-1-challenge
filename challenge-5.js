function getSplitName(personName) {
  if (typeof personName === "string") {
    // pemisahan pada string personName berdasarkan spasi, membagi string tersebut menjadi array
    const name = personName.split(" ");

    if (name.length <= 3) {
      return {
        firstName: name[0],
        middleName:  name.length === 3 ? name[1] : null ,
        lastName: name.length === 2 ? name[1] : name[2] || null,
      };
    } else {
      return "Error: This function is only for 3 character name";
    }
  } else {
    return "Error: invalid data type";
  }
}

console.log(getSplitName("Hindun binti Utbah"));

console.log(getSplitName("Miracle Ileana"));

console.log(getSplitName("Roxana"));

console.log(getSplitName("Hiroto Natsuko Kenichi Mari"));

console.log(getSplitName(0));
