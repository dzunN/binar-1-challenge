function getAngkaTerbesarKedua(dataNumbers) {

  if (!Array.isArray(dataNumbers)) { // => jika parameter yang diterima bukan array
    return "Error: Please insert the number correctly! this input only receive array data";
  } else if (dataNumbers.length <= 1) { // cek apakah dataNumbers.length kurang sama dengan 1
    return "Error: Please insert 2 or more number!"
  }  else {

    // Mengurutkan dan membalikkan angka => menggunakan sort dengan memberi compare function 
    dataNumbers.sort((firstEl,secondEl) => secondEl - firstEl);
    
    // Mengambil angka terbesar
    let angkaTerbesar = Math.max(...dataNumbers); // => Output: 9

    // Mengambil total angka terbesar
    let totalAngkaTerbesar = dataNumbers.filter((dataNumbers) => dataNumbers === angkaTerbesar).length // => Output: 2

    // Berarti angkaTerbesarKedua ada di index ke 2 (totalAngkaTerbesar = 2 jadi geser index ke 2) 
    return dataNumbers[totalAngkaTerbesar];
  }
}

const dataAngka = [9,9, 4, 7, 7, 5, 3, 2, 2, 6];


console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());
