function isValidPassword(givenPassword) {
  // inisialisasi format password
  const passwordFormat = /(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z])(?=.{8})/;

  if(typeof givenPassword === "string"){
    if (passwordFormat.test(givenPassword)) {
      return true;
    } else {
      return false;
    }
  }else{
    return givenPassword === undefined ? "Error: Password harus diisi!" : "Error : invalid data type"
  }
}

console.log(isValidPassword("Ohayo2022"));

console.log(isValidPassword("ohayo2022"));

console.log(isValidPassword("@hayo"));

console.log(isValidPassword("Ohayo2"));

console.log(isValidPassword(0)); //akan error, karena nilai yang di masukkan tidak berupa string

console.log(isValidPassword()); // <= Akan return undefined, karena fungsi "isValidPassword()" tidak menerima nilai atau password tidak diinputkan.
